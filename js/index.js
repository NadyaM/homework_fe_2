let slideDownMenu = $('.nav__menu > div:not(:first-of-type)');
let slideDownText = $('.nav__menu > div:not(:first-of-type) span');
let menuButton = $('.nav__button-wrapper');
let menuLines = $('.nav__button-wrapper div');
let clickCount = 0;
let checkHeight;

$('body').click(function (e) {
   if (e.target.id === 'button_off_click' || $(e.target).hasClass('menu_off_click')) {
       let viewportWidth = $(window).width();
       if (viewportWidth < 640) {
           checkHeight = '161px';
       } else if (viewportWidth < 980 && viewportWidth >= 640) {
           checkHeight = '235px';
       }
       $(menuLines[0]).toggleClass('menu_click_disappear');
       $(menuLines[2]).toggleClass('menu_click_rotate');
       $(menuButton).toggleClass('menu_click_wrapper_rotate');
       if (clickCount === 0) {
           $(slideDownMenu).wrapAll(`<div class="slide_menu_switch" />`);
           $('.slide_menu_switch').toggleClass('slide_menu_switch').toggleClass('slide_menu').animate({
               height: `${checkHeight}`,
           }, {
               duration: 400,
               easing: "linear",
           });
           clickCount++;
           $(slideDownMenu).toggleClass('slide_menu_items');
           $(slideDownText).toggleClass('slide_menu_margin');
       } else {
           $('.slide_menu').toggleClass('slide_menu_switch').toggleClass('slide_menu').animate({
               height: "0px",
           }, {
               duration: 400,
               easing: "linear",
           });
           clickCount--;
           $(slideDownMenu).unwrap();
           $(slideDownMenu).toggleClass('slide_menu_items');
           $(slideDownText).toggleClass('slide_menu_margin');
       }
   } else {
       if (clickCount === 1) {
           $(menuLines[0]).toggleClass('menu_click_disappear');
           $(menuLines[2]).toggleClass('menu_click_rotate');
           $(menuButton).toggleClass('menu_click_wrapper_rotate');
           $('.slide_menu').toggleClass('slide_menu_switch').toggleClass('slide_menu').animate({
               height: "0px",
           }, {
               duration: 400,
               easing: "linear",
           });
           clickCount--;
           $(slideDownMenu).unwrap();
           $(slideDownMenu).toggleClass('slide_menu_items');
           $(slideDownText).toggleClass('slide_menu_margin');
       }
   }
});
